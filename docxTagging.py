# !/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@filename:	docxTagging.py
@author:	LearningPawn5
@created:	2021-03-28
@updated:	2021-03-28
"""
try:
    import docx
except ImportError:
    print("您似乎还未安装 ‘python-docx’ 库")
    print("在命令行输入 ‘pip install python-docx’ 来安装")
    choose = input("或立即尝试安装([y]/n)：")
    if choose == 'n':
        pass
    else:
        import os
        os.system('pip install python-docx')


def replace_docx_format(path, dicts, formats=None, output=None):
    """针对word文档中指定的内容进行格式的替换。

    针对Word文档指定内容的查找与替换，其功能与高级的Ctrl+H差不多，部分代码参考：
    https://blog.csdn.net/qq_34474585/article/details/85092751
    本代码仅供学习使用。

    Args:
        :param path: 文件路径，不可为空。
        :param dicts: 用于查找的字典，不可为空。
        :param formats: 具体目标格式。（默认为只加下划线）
        :param output: 格式化之后的保存到新文件的路径。（默认为原路径）

    Returns:
        输出查找的结果，如果为True则表示找到并替换成功，
        如果为False则表示未找到指定内容。

    Raises:
        ImportError: 缺少必要的库。
        PackageNotFoundError: 输入的文件路径有错，没有找到指定文件。
        TypeError: 缺少必要的参数或其他错误。
    """
    if not output:
        print('[提示] 已启用默认输出：新文件将会尝试覆盖源文件。')
        output = path
    if '.docx' not in path or '.docx' not in output:
        print('[错误] 请使用docx文件。')
        return False
    if not formats:
        print('[提示] 已启用默认格式：为找到的文字变红加粗并添加下划线。')
        formats = ['underline', 'bold', 'red']

    count = 0
    try:
        file = docx.Document(path)
    except docx.opc.exceptions.PackageNotFoundError:
        print('[错误] 在路径 ', path, ' 下未找到指定文件！')
        return False
    for paragraph in file.paragraphs:
        for run in paragraph.runs:
            for key in dicts:
                if key in run.text:
                    count += 1
                    # if key == run.text:
                    run = format_docx_runs(run, formats)
                    # else:
                    #     此处需将关键字从paragraph的run中分割出来，
                    #     格式化后再重新加入paragraph中，否则，
                    #     将有可能格式化一整个词组。
    if count == 0:
        print('[警告] 在文档 ', path, ' 中未找到指定内容！未进行保存。')
        return False
    try:
        file.save(output)
    except IOError:
        print('[错误] 请先关闭output的目标文件。')
    print('[提示] 已找到 ', count, ' 处需要替换的内容，并保存至 ', output)


def format_docx_runs(runs, formats):
    if 'underline' in formats:
        runs.underline = True
    if 'bold' in formats:
        runs.bold = True
    if 'red' in formats:
        runs.font.color.rgb = docx.shared.RGBColor(250, 0, 0)
    return runs


if __name__ == '__main__':	# 主函数（测试用）
    #   Example:
    #       检查与本文件同目录下的document.docx文件中的Hello和World单词，如果存在，则替换为默认标注格式，并输出到源文件
    replace_docx_format('./document.docx', ['Hello', 'World'])
	
	#   Example:
    #       检查与本文件同目录下的document.docx文件中是否含有同目录下的‘中国地名.csv’中的地名信息，如果存在，则替换为默认标注格式，并输出到同路径下的document(after).docx
    try:
        import csv
    except ImportError:
        print("您似乎还未安装 ‘csv’ 库")
        print("在命令行输入 ‘pip install csv’ 来安装")
        choose = input("或立即尝试安装([y]/n)：")
        if choose == 'n':
            pass
        else:
            import os
            os.system('pip install csv')
    with open('./中国地名.csv') as f:
        dicts = list(csv.reader(f))
    replace_docx_format('./document.docx', dicts[0], output='./document(after).docx')
