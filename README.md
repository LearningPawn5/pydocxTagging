# 基于python-docx的关键词标注函数
 python对docx文件的一种操作，具体这个docx是怎么运行的，还没看太明白。
 
## 要求
 1. 安装python3.x；
 2. pip或anaconda安装必须的库：
  - pip install python-docx
  - pip install csv
 3. 按照需要修改替换主函数中的内容。
 
## 运行
 1. 在安装完python3.x之后，使用Linux terminal或者是Windows cmd.exe打开本文件所在路径，使用`python docxTagging.py`来运行文件；
 2. 回到本文件所在路径，检查是否已经有文件输出。
 
## 提示
 - 本函数的主函数中提供了两个示例：
  1. 检查与本文件同目录下的document.docx文件中的Hello和World单词，如果存在，则替换为默认标注格式，并输出到源文件；
  2. 检查与本文件同目录下的document.docx文件中是否含有同目录下的‘中国地名.csv’中的地名信息，如果存在，则替换为默认标注格式，并输出到同路径下的document(after).docx
 - 由于本函数同路径下的‘中国地名.csv’文件中提供的是全国的所有地名（精确到县），且没有别称和缩写。
 如有需要可以自行添加去除了‘省’、‘市’、‘区’等后缀的地名到csv或python文件的list中。
 